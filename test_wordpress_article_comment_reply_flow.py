import time
import pytest
from base64 import b64encode
import requests
import lorem


# GLOBAL VARIABLES
username = 'editor'
password = 'HWZg hZIP jEfK XCDE V9WM PQ3t'
blog_url = 'https://gaworski.net'
posts_endpoint_url = blog_url + "/wp-json/wp/v2/posts"
token = b64encode(f"{username}:{password}".encode('utf-8')).decode("ascii")

username_commenter = 'commenter'
password_commenter = 'SXlx hpon SR7k issV W2in zdTb'
comments_endpoint_url = blog_url + "/wp-json/wp/v2/comments"
token_commenter = b64encode(f"{username_commenter}:{password_commenter}".encode('utf-8')).decode("ascii")
general_time_stamp = int(time.time())

@pytest.fixture(scope='module')
def article():
    article = {
        "title": "Article created by LR " + str(general_time_stamp),
        "content": "tekst napisany dla testu - content",
        "excerpt": lorem.sentence(),
        "status": "publish"
    }
    return article


@pytest.fixture(scope='module')
def headers_user():
    header_user = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token
    }
    return header_user

@pytest.fixture(scope='module')
def headers_comment():
    header_comment = {
        "Content-Type": "application/json",
        "Authorization": "Basic " + token_commenter
    }
    return header_comment

@pytest.fixture(scope='module')
def comment():
    comment = {
        "post": None,
        "content": "tekst napisany dla testu - content-comment"
    }
    return comment
@pytest.fixture(scope='module')
def reply_to_commenter():
    reply_to_commenter = {
        "post": None,
        "parent": None,
        "content": "tekst napisany dla testu - content comment"
    }
    return reply_to_commenter

@pytest.fixture(scope='module')
def create_response_article(headers_user, article):
    post_requests = requests.post(url=posts_endpoint_url, headers=headers_user, json=article, verify=False)
    return post_requests

# Test case
def test_add_post_comment__reply(article, comment, headers_user, headers_comment, reply_to_commenter, create_response_article):

    assert create_response_article.status_code == 201
    assert create_response_article.reason == "Created"
    assert create_response_article.url == posts_endpoint_url

    article_id = create_response_article.json()["id"]
    comment['post'] = article_id

    response_get_article = requests.get(f'{posts_endpoint_url}/{article_id}')
    assert response_get_article.status_code == 200
    article_json = response_get_article.json()
    assert article_json["type"] == 'post'
    assert article_json["status"] == 'publish'

    comment_requests = requests.post(url=comments_endpoint_url, headers=headers_comment, json=comment, verify=False)
    assert comment_requests.status_code == 201

    comment_id = comment_requests.json()["id"]
    reply_to_commenter["post"] = article_id
    reply_to_commenter["parent"] = comment_id

    response_get_comment = requests.get(f'{comments_endpoint_url}/{comment_id}')
    assert response_get_comment.status_code == 200
    comment_json = response_get_comment.json()
    assert comment_json["post"] == article_id
    assert comment_json["author_name"] == 'Albert Commenter'
    assert comment_json["type"] == 'comment'
    assert comment_json["status"] == 'approved'


    reply_requests = requests.post(url=comments_endpoint_url, headers=headers_user, json=reply_to_commenter, verify=False)
    assert reply_requests.status_code == 201
    reply_id = reply_requests.json()['id']

    reply_get_comment = requests.get(f"{comments_endpoint_url}/{reply_id}")
    assert response_get_comment.status_code == 200
    reply_json = reply_get_comment.json()
    assert reply_json['post'] == article_id
    assert reply_json['parent'] == comment_id
    assert reply_json['author_name'] == 'John Editor'
    assert reply_json['type'] == 'comment'
